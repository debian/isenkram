Modalias decoded
================

This document try to explain what the different types of modalias
values stands for.  It is in part based on information from
<URL: https://wiki.archlinux.org/index.php/Modalias >,
<URL: http://unix.stackexchange.com/questions/26132/how-to-assign-usb-driver-to-device >,
<URL: http://code.metager.de/source/history/linux/stable/scripts/mod/file2alias.c > and
<URL: http://cvs.savannah.gnu.org/viewvc/dmidecode/dmidecode.c?root=dmidecode&view=markup >.

The modalias entries for a given Linux machine can be found using this
shell script:

  find /sys/devices -name modalias -print0 | xargs -0 sort -u

The supported modalias globs for a given kernel module can be found
using modinfo:

  % /sbin/modinfo psmouse | grep alias:
  alias:          serio:ty05pr*id*ex*
  alias:          serio:ty01pr*id*ex*
  %


PCI subtype
-----------

A typical PCI entry can look like this.  This is an Intel Host Bridge
memory controller:

  pci:v00008086d00002770sv00001028sd000001ADbc06sc00i00

This represent these values:

 v   00008086  (vendor)
 d   00002770  (device)
 sv  00001028  (subvendor)
 sd  000001AD  (subdevice)
 bc  06        (bus class)
 sc  00        (bus subclass)
 i   00        (interface)

The vendor/device values are the same values outputted from 'lspci -n'
as 8086:2770.  The bus class/subclass is also shown by lspci as 0600.
The 0600 class is a host bridge.  The complete list can be found at
<URL: https://pci-ids.ucw.cz/read/PD >:

  0200 Ethernet controller
  0300 VGA compatible controller
  0600 Host bridge
  0700 Serial controller

Not sure how to figure out the interface value, nor what it means.

USB subtype
-----------

Some typical USB entries can look like this.  This is an internal USB
hub in a laptop:

  usb:v1D6Bp0001d0206dc09dsc00dp00ic09isc00ip00

Here is the values included in this alias:

 v    1D6B  (device vendor)
 p    0001  (device product)
 d    0206  (bcddevice)
 dc     09  (device class)
 dsc    00  (device subclass)
 dp     00  (device protocol)
 ic     09  (interface class)
 isc    00  (interface subclass)
 ip     00  (interface protocol)

The 0900 device class/subclass means hub.  Some times the relevant
class is in the interface class section.  For a simple USB web camera,
these alias entries show up:

  usb:v0AC8p3420d5000dcEFdsc02dp01ic01isc01ip00
  usb:v0AC8p3420d5000dcEFdsc02dp01ic01isc02ip00
  usb:v0AC8p3420d5000dcEFdsc02dp01ic0Eisc01ip00
  usb:v0AC8p3420d5000dcEFdsc02dp01ic0Eisc02ip00

Interface class 0E01 is video control, 0E02 is video streaming (aka
camera), 0101 is audio control device and 0102 is audio streaming (aka
microphone).  Thus this is a camera with microphone included.

Here are some USB device and interface classes.  The complete list can
be found at <URL: https://usb-ids.gowdy.us/read/UC/ >:

  01XX  Audio
  07XX  Printer
  09XX  HUB
  0Bxx  Chip/SmartCard
  0EXX  Video
  E0XX  Wireless
  EFXX  Miscellaneous Device

ACPI subtype
------------

The ACPI type is used for several non-PCI/USB stuff.  This is an IR
receiver in a Thinkpad X40:

  acpi:IBM0071:PNP0511:

The values between the colons are IDs.

DMI subtype
-----------

The DMI table contain lots of information about the computer case and
model.  This is an entry for a IBM Thinkpad X40, fetched from
/sys/devices/virtual/dmi/id/modalias:

  dmi:bvnIBM:bvr1UETB6WW(1.66):bd06/15/2005:svnIBM:pn2371H4G:pvrThinkPadX40:rvnIBM:rn2371H4G:rvrNotAvailable:cvnIBM:ct10:cvrNotAvailable:

The values present are

 bvn  IBM            (BIOS vendor)
 bvr  1UETB6WW(1.66) (BIOS version)
 bd   06/15/2005     (BIOS date)
 svn  IBM            (system vendor)
 pn   2371H4G        (product name)
 pvr  ThinkPadX40    (product version)
 rvn  IBM            (board vendor)
 rn   2371H4G        (board name)
 rvr  NotAvailable   (board version)
 cvn  IBM            (chassis vendor)
 ct   10             (chassis type)
 cvr  NotAvailable   (chassis version)

The chassis type 10 is Notebook.  Other interesting values can be
found in the dmidecode source:

  3 Desktop
  4 Low Profile Desktop
  5 Pizza Box
  6 Mini Tower
  7 Tower
  8 Portable
  9 Laptop
 10 Notebook
 11 Hand Held
 12 Docking Station
 13 All In One
 14 Sub Notebook
 15 Space-saving
 16 Lunch Box
 17 Main Server Chassis
 18 Expansion Chassis
 19 Sub Chassis
 20 Bus Expansion Chassis
 21 Peripheral Chassis
 22 RAID Chassis
 23 Rack Mount Chassis
 24 Sealed-case PC
 25 Multi-system
 26 CompactPCI
 27 AdvancedTCA
 28 Blade
 29 Blade Enclosing

The chassis type values are not always accurately set in the DMI
table.  For example my home server is a tower, but the DMI modalias
claim it is a desktop.

SerIO subtype
-------------

This type is used for PS/2 mouse plugs.  One example is from my test
machine:

  serio:ty01pr00id00ex00

The values present are

  ty  01  (type)
  pr  00  (prototype)
  id  00  (id)
  ex  00  (extra)

This type is supported by the psmouse driver.  I am not sure what the
valid values are.

Other subtypes
--------------

There are heaps of other modalias subtypes according to file2alias.c.
There is the rest of the list from that source: amba, ap, bcma, ccw,
css, eisa, hid, i2c, ieee1394, input, ipack, isapnp, mdio, of, parisc,
pcmcia, platform, scsi, sdio, spi, ssb, vio, virtio, vmbus, x86cpu and
zorro.  I did not spend time documenting all of these, as they do not
seem relevant for my intended use with mapping hardware to packages
when new stuff is inserted during run time.

Looking up kernel modules using modalias values
-----------------------------------------------

To check which kernel modules provide support for a given modalias,
one can use the following shell script:

  find /sys/devices -name modalias -print0 | xargs -0 sort -u | while read -r id; do
    echo "$id"
    /sbin/modprobe --show-depends "$id" | sed 's/^/  /'
  done

The output can look like this (only the first few entries as the list
is very long on my test machine):

  acpi:ACPI0003:
    insmod /lib/modules/2.6.32-5-686/kernel/drivers/acpi/ac.ko 
  acpi:device:
  FATAL: Module acpi:device: not found.
  acpi:IBM0068:
    insmod /lib/modules/2.6.32-5-686/kernel/drivers/char/nvram.ko 
    insmod /lib/modules/2.6.32-5-686/kernel/drivers/leds/led-class.ko 
    insmod /lib/modules/2.6.32-5-686/kernel/net/rfkill/rfkill.ko 
    insmod /lib/modules/2.6.32-5-686/kernel/drivers/platform/x86/thinkpad_acpi.ko 
  acpi:IBM0071:PNP0511:
    insmod /lib/modules/2.6.32-5-686/kernel/lib/crc-ccitt.ko 
    insmod /lib/modules/2.6.32-5-686/kernel/net/irda/irda.ko 
    insmod /lib/modules/2.6.32-5-686/kernel/drivers/net/irda/nsc-ircc.ko 
  [...]
