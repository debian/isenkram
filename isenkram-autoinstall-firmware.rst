===============================
 isenkram-autoinstall-firmware
===============================

--------------------------------------------------------------
user desktop background process to handle new hardware dongles
--------------------------------------------------------------

:Author: Petter Reinholdtsen - pere@hungry.com
:Date:   2017-09-01
:Copyright: GPL v2+
:Version: 0.0
:Manual section: 8
:Manual group: User Commands

SYNOPSIS
========

isenkram-autoinstall-firmware

DESCRIPTION
===========

Provide easy way to install required firmware needed by Linux kernel
modules.  It tries to identify missing firmware, and the packages to
install to get the firmware.

OPTIONS
=======

There are no options.

SEE ALSO
========

* isenkramd(1), isenkram-pkginstall(8)
